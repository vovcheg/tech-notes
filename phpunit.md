```bash
build/vendor/bin/phpunit --debug --filter=HK_CustomerRegistration_Test --coverage-text
```

```bash
./phpunit --debug --filter=HK_CustomerRegistration_Test --coverage-html coverage-registration
#The coverage html can be accessed in ProjectSourceDir/coverage-registration/index.html
```