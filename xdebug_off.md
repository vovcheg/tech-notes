**dpd**
```bash
sudo vim /usr/local/bin/xdebug;
```

```bash
#!/bin/bash

# Simple script to enable or disable the xdebug extension

case $1 in
  on)
    [ -f /etc/php/7.1/mods-available/xdebug.ini.deactivated ] && sudo mv /etc/php/7.1/mods-available/xdebug.ini.deactivated /etc/php/7.1/mods-available/xdebug.ini
    sudo service apache2 restart
  ;;
  off)
    [ -f /etc/php/7.1/mods-available/xdebug.ini ] && sudo mv /etc/php/7.1/mods-available/xdebug.ini /etc/php/7.1/mods-available/xdebug.ini.deactivated
    sudo service apache2 restart
  ;;
  *)
    echo "Usage: xdebug on|off"
  ;;
esac
```
`sudo chmod +x /usr/local/bin/xdebug`

**nkd:**

```bash
#!/bin/bash

# Simple script to enable or disable the xdebug extension

case $1 in
  on)
    [ -f /etc/php5/apache2/conf.d/20-xdebug.ini.deactivated ] && sudo mv /etc/php5/apache2/conf.d/20-xdebug.ini.deactivated /etc/php5/apache2/conf.d/20-xdebug.ini
    sudo service apache2 restart
  ;;
  off)
    [ -f /etc/php5/apache2/conf.d/20-xdebug.ini ] && sudo mv /etc/php5/apache2/conf.d/20-xdebug.ini /etc/php5/apache2/conf.d/20-xdebug.ini.deactivated
    sudo service apache2 restart
  ;;
  *)
    echo "Usage: xdebug on|off"
  ;;
esac
```
```