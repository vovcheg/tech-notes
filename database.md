[Index](https://bitbucket.org/vovcheg/tech-notes)

**create missing user**
```sh
GRANT ALL ON *.* TO 'bw_disney'@'%' IDENTIFIED BY 'complex-password';
FLUSH PRIVILEGES;
php


pendants_db
```
**replace URL**
```sql
update core_config_data set value = replace(value, "https://uat.buyerquest.net/pbsd/", "http://93.77.1.94:8888/pbsd/");
update core_config_data set value = replace(value, "https://bravo.atwix.com:6227/", "http://bravo.atwix.com:4227/");
update core_config_data set value = replace(value, "https://shop.hypervsn.com/", "http://alpha.atwix.com:4211");
update core_config_data set value = replace(value, "head.localhost", "head.atwix.com");
```
**list table size**
```sql
SELECT
  table_name AS                                          "Tables",
  round(((data_length + index_length) / 1024 / 1024), 2) "Size in MB"
FROM information_schema.TABLES
WHERE table_schema = "percent-prod"
ORDER BY (data_length + index_length) DESC;
```
**table size and igonre**
```sql
SELECT
  TABLE_NAME,
  concat('--ignore-table=',table_schema, '.', TABLE_NAME )
    table_rows,
  data_length,
  index_length,
  round(((data_length + index_length) / 1024 / 1024), 2) "Size in MB"
FROM information_schema.TABLES
WHERE table_schema = "starshop_qa"
ORDER BY (data_length + index_length) DESC;
```
**rename database**
```sh
mysqldump -u root -v wfs_catalog -p | mysql -u root -p -D wfs_catalog_bak
```
**pipe mysql**
```
mysqldump  -h 192.168.99.100 -P 3309 -u root  -p'Y111111' starshop_qa | mysql -h 127.0.0.1 -u root starshop_qa
```

**delete all tables from DB**
```
SET FOREIGN_KEY_CHECKS = 0;

select concat('drop table if exists ', table_name, ' cascade;')
  from information_schema.tables
 where table_schema = 'orotest1';

SET FOREIGN_KEY_CHECKS = 1;
```


**skip table on import**
```sh
sed '/INSERT INTO `TABLE_TO_SKIP`/d' DBdump.sql > reduced.sql

sed '/INSERT INTO `TABLE1_TO_SKIP`/d' DBdump.sql | \
sed '/INSERT INTO `TABLE2_TO_SKIP`/d' | \
sed '/INSERT INTO `TABLE3_TO_SKIP`/d' > reduced.sql
```

**db backup**
```sh
mysqldump -h generic-dev-56.c9c29krzsy4z.us-east-1.rds.amazonaws.com -u starshop_user -p'm4G0xajEfa$' magento | gzip > var/`date -I`.database.sql.gz
mysqldump -u root komodo | gzip > var/`date -I`.komodo.sql.gz
```
**db restore**
```sh
gzip -dc < 2015-09-24.database.sql.gz | mysql -h 192.168.99.100 -P 3309 -u root  -p'Y111111' starshop_dev2
gzip -dc < restore.sql.gz | mysql -h 172.24.16.45 -f -u headcovers  -p headcovers
```


**strip database when export**
```bash
n98-magerun.phar db:dump --strip="dataflow_batch_export report* customer_emails @log @stripped @sales"  -c gzip
n98-magerun.phar db:dump --strip="@log @stripped @sales"  -c gzip
n98-magerun.phar db:dump --strip="dataflow_batch_export report* customer_emails @log @stripped @sales @customers @trade @development @admin @emails @newsletter"  -c gzip
n98-magerun.phar db:dump --strip="report* @development @idx *tmp"  -c gzip

```