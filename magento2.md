[Index](https://bitbucket.org/vovcheg/tech-notes)


**php extensions**
```bash
sudo apt-get install php7.0-xml php7.0-gd php7.0-intl php7.0-mbstring php7.0-mcrypt php7.0-opcache  php7.0-soap php7.0-zip php7.0-mysql php7.0-curl php7.0-xdebug php7.0-bcmath
sudo apt-get install php7.1-xml php7.1-gd php7.1-intl php7.1-mbstring php7.1-mcrypt php7.1-opcache  php7.1-soap php7.1-zip php7.1-mysql php7.1-curl php7.1-xdebug php7.1-bcmath
sudo apt-get install php7.1-xml php7.1-gd php7.1-intl php7.1-mbstring php7.1-mcrypt php7.1-opcache  php7.1-soap php7.1-zip php7.1-mysql php7.1-curl php7.1-xdebug php7.1-bcmath
sudo apt-get install php7.2-xml php7.2-gd php7.2-intl php7.2-mbstring php7.2-opcache  php7.2-soap php7.2-zip php7.2-mysql php7.2-curl php7.2-bcmath php7.2-xdebug php7.2-mcrypt
sudo apt-get install php5.6-xml php5.6-gd php5.6-intl php5.6-mbstring php5.6-opcache  php5.6-soap php5.6-zip php5.6-mysql php5.6-curl php5.6-bcmath php5.6-xdebug php5.6-mcrypt
sudo apt-get install php7.1-xdebug php7.1-cli php7.1-common php7.1-json php7.1-opcache php7.1-mysql php7.1-phpdbg php7.1-mbstring php7.1-gd php7.1-imap php7.1-ldap php7.1-pgsql php7.1-pspell php7.1-recode php7.1-snmp php7.1-tidy php7.1-dev php7.1-intl php7.1-gd php7.1-curl php7.1-zip php7.1-xml
sudo apt-get install php7.2-xdebug php7.2-cli php7.2-common php7.2-json php7.2-opcache php7.2-mysql php7.2-phpdbg php7.2-mbstring php7.2-gd  php7.2-intl php7.2-gd php7.2-curl php7.2-zip php7.2-xml php7.2-bcmath php7.2-xml php7.2-gd php7.2-intl php7.2-mbstring php7.2-opcache  php7.2-soap php7.2-zip php7.2-mysql php7.2-curl
```
**composer create project command**
```bash
composer create-project --repository-url=https://repo.magento.com/ magento/project-community-edition --stability=beta --ignore-platform-reqs m2
composer create-project --repository=https://repo.magento.com/ magento/project-community-edition mage2.2.6
```
or
```
composer create-project --repository-url=https://repo.magento.com/ magento/project-community-edition --stability=dev --ignore-platform-reqs ./
```

**EE commerce**
```bash
composer create-project --repository-url=https://repo.magento.com/ magento/project-enterprise-edition
```

**install Magento commerce edition with sample data* using composer*

 - `composer create-project --repository-url=https://repo.magento.com/ magento/project-enterprise-edition=2.2.5`
 
 
 - `composer create-project --repository-url=https://repo.magento.com/ magento/project-enterprise-edition`
 - `bin/magento sampledata:deploy`
 - `username:2ec19b1f1ace8707819ad1f868f338ec pass:1ae985946999622369facee0dec18919` 
 - `bin/magento setup:install --db-name=kinomo_cloud --db-password=111 --db-user=root --base-url="http://bravo.atwix.com:1701/" --admin-user="atwix" --admin-password="Y111111" --admin-email="ext@atwix.com" --admin-firstname="Atwix" --admin-lastname="Team"`
 - `bin/magento setup:install --db-name=kinomo_cloud --db-user=root --base-url="http://dev.m2commerce.com:8080/" --admin-user="atwix" --admin-password="Y111111" --admin-email="ext@atwix.com" --admin-firstname="Atwix" --admin-lastname="Team"`
 - `bin/magento setup:install --db-name=graphql --db-user=root --db-password=111 --base-url="http://bravo.atwix.com:4212/graphql/" --admin-user="atwix" --admin-password="Y111111" --admin-email="ext@atwix.com" --admin-firstname="Atwix" --admin-lastname="Team"`
 - `bin/magento setup:install --db-name=mage226 --db-user=root --db-password=111 --base-url="http://bravo.atwix.com:4212/mage2.2.6/" --admin-user="atwix" --admin-password="Y111111" --admin-email="ext@atwix.com" --admin-firstname="Atwix" --admin-lastname="Team"`
 - `bin/magento setup:install --db-name=commerce --db-user=root --db-password=111 --base-url="http://alpha.atwix.com:4200/commerce/" --admin-user="atwix" --admin-password="Y111111" --admin-email="ext@atwix.com" --admin-firstname="Atwix" --admin-lastname="Team"`
 - `bin/magento setup:install --db-name=fresh --db-user=root --db-password=111 --base-url="https://bravo.atwix.com:6226/" --admin-user="atwix" --admin-password="Y111111" --admin-email="ext@atwix.com" --admin-firstname="Atwix" --admin-lastname="Team"`
 
 
**set permissions**
```bash
cd <your Magento install dir>
find var vendor pub/static pub/media app/etc -type f -exec chmod g+w {} \;
find var vendor pub/static pub/media app/etc -type d -exec chmod g+ws {} \;
chown -R :www-data .
chmod u+x bin/magento
```

**install command**
```
bin/magento setup:install --db-name=m2 --db-user=root --base-url="https://ubulap.com/" --admin-user="vovsky" --admin-password="" --admin-email="vovskyi@gmail.com" --admin-firstname="Volodymyr" --admin-lastname="V"
```
or 
```bash
bin/magento setup:install --db-name=m2ee --db-user=root --base-url="http://ubulap.com:8000/" --admin-user="vovsky" --admin-password="" --admin-email="vovskyi@gmail.com" --admin-firstname="Volodymyr" --admin-lastname="V" --admin-password="140288vvp"
```
```bash
bin/magento setup:install --db-name=m2ee --db-user=root --base-url="http://bravo.atwix.com:4214/" --admin-user="atwix" --admin-password="Y111111" --admin-email="vovsky@atwix.com" --admin-firstname="Volodymyr" --admin-lastname="V"
```
```bash
bin/magento setup:install --db-name=msi --db-password=111 --db-user=root --base-url="http://bravo.atwix.com:4203/msi/" --admin-user="atwix" --admin-password="Y111111" --admin-email="vovskyi@atwix.com" --admin-firstname="Volodymyr" --admin-lastname="V" 
bin/magento setup:install --db-name=msi --db-password=111 --db-user=root --base-url="http://dev.msi.com:4215/" --admin-user="atwix" --admin-password="Y111111" --admin-email="vovskyi@atwix.com" --admin-firstname="Volodymyr" --admin-lastname="V"

bin/magento setup:install --db-name=m2ee --db-user=root --base-url="http://ubulap.com:8000/" --admin-user="vovsky" --admin-password="" --admin-email="vovskyi@gmail.com" --admin-firstname="Volodymyr" --admin-lastname="V" --admin-password="140288vvp" 
bin/magento setup:install --db-name=dev.mage23.com --db-user=root --db-password=root  --base-url="http://dev.mage23.com:8080/" --admin-user="vovsky" --admin-password="Y111111" --admin-email="vovskyi@gmail.com" --admin-firstname="Volodymyr" --admin-lastname="V"  
bin/magento setup:install \
--db-name=mage231 \
--db-user=root \
--db-password=111  \
--base-url="http://alpha.atwix.com:4219/" \
--admin-user="atwix" \
--admin-password="Y111111" \
--admin-email="ext@atwix.com" \
--admin-firstname="Atwix" \
--admin-lastname="Team" \
--use-rewrites=1  
```

####update magento2 beta/dev:
* switch to develop branch
* `composer install`
* `bin/magento setup:upgrade`


####events
```php
//file generated/code/Magento/Framework/Event/Manager/Proxy.php
$request = $this->_objectManager->get(\Magento\Framework\App\RequestInterface::class);
        $suffix = '';
        if ($eventName == 'view_block_abstract_to_html_before'){
            /** @var \Magento\Framework\View\Element\AbstractBlock $block */
            $block = $data['block'];
            $suffix = $block->getNameInLayout();
        }

        \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Psr\Log\LoggerInterface::class)->debug($request->getRequestUri() . ' '. $eventName . ' ' . $suffix);
```

**admin user create**
```bash
bin/magento admin:user:create --admin-user atwix --admin-firstname atwix --admin-lastname atwix --admin-email ext@atwix.com --admin-password Y111111
bin/magento admin:user:create --admin-user vovsky --admin-firstname atwix --admin-lastname atwix --admin-email v.vygovskyi@atwix.com --admin-password yg4pmS-Q285hJ*ER
bin/magento admin:user:create --admin-user aw --admin-firstname igor --admin-lastname support --admin-email support@aw.com --admin-password support123"
bin/magento admin:user:create --admin-user 4tellTina --admin-firstname 4tell --admin-lastname support --admin-email support@4tell.com --admin-password 4t1n4Rx$47u
```

**change customer password**
```sql
SET @email='vovsky@atwix.com', @passwd='Y111111', @salt=MD5(RAND());

UPDATE customer_entity
    SET password_hash = CONCAT(SHA2(CONCAT(@salt, @passwd), 256), ':', @salt, ':1')
    WHERE email = @email;
```

**run unit test**
```bash
vendor/bin/phpunit -c dev/tests/unit/phpunit.xml.dist --coverage-text app/code/LiebherrLps/CatalogPermissionsIntegration/Test/Unit

```
**logging**
```php
$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
$logger = new \Zend\Log\Logger();
$logger->addWriter($writer);
$logger->info('Your text message');
```


**tunnel to rabbitmq**
```bash
ssh -L 15672:localhost:15672 dev@127.0.0.1 -p 2020 -i ~/.ssh/id_rsa_atwix
ssh -L 15672:localhost:15672 dev@alpha.atwix.com -p 5210 -i ~/.ssh/id_rsa_atwix
ssh -L 5000:localhost:5000 dev@bravo.atwix.com -p 5219 -i ~/.ssh/id_rsa_atwix
```

**magento cloud install command**
```bash
php magento setup:install \
   --admin-firstname=Atwix \
   --admin-lastname=Team \
   --admin-email=ext@atwix.com \
   --admin-user=admin \
   --admin-password=Y111111 \
   --base-url=http://alpha.atwix.com:4211/ \
   --db-host=127.0.0.1 \
   --db-name=new \
   --db-user=root \
   --db-password=111 \
   --currency=USD \
   --language=en_US \
   --use-rewrites=1
```

**set config values with command line**
```bash
bin/magento config:set --lock web/unsecure/base_url http://alpha.atwix.com:4211/ && bin/magento config:set --lock web/secure/base_url http://alpha.atwix.com:4211/ && bin/magento config:set --lock admin/security/use_form_key 0 && bin/magento config:set --lock admin/security/session_lifetime 31536000 && bin/magento config:set --lock admin/security/password_lifetime ''
bin/magento config:set --lock admin/security/use_form_key 0 && bin/magento config:set --lock admin/security/session_lifetime 31536000 && bin/magento config:set --lock admin/security/password_lifetime ''
bin/magento config:set  admin/security/use_form_key 0 && bin/magento config:set  admin/security/session_lifetime 31536000 && bin/magento config:set admin/security/password_lifetime ''
bin/magento app:config:import
```

export XDEBUG_CONFIG="idekdey=PHPSTORM"


**config set**

```bash
bin/magento config:set admin/security/use_form_key 0 && \
bin/magento config:set admin/security/session_lifetime 31536000 && \
bin/magento config:set admin/security/password_lifetime '' && \
bin/magento config:set admin/security/admin_account_sharing 1 



```

**no route / 404 **
```bash
\Magento\Framework\Controller\Result\Forward::forward
\Magento\Framework\Controller\Result\Forward
```