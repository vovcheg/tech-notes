FPC - liebherr catalog permissions

before order place, check shipping - liebher b2b, `\Liebherr\Shipping\Plugin\CheckoutShippingInformationManagementPlugin`, `beforeSaveAddressInformation`

**adding a carrier with additional info rendered on a frontend** - LIEB-32

**load block by ajax, render handle** - HK-32

**M2 adding shipping carriers**  - LIEB-33 Task #34545
**create the logger M2** - TJS-53

**JS component, widget** - Lieb lps 34301

**Admin js widget bind** - DPD-327 - `https://github.com/Atwix/DPD-Shipping-M2/commit/cf3b046f1e0a22381b6d3a8192977726585568d8`

**coupon pool**  - NKD-1
** form validation, js elements**  - DPD-280 https://github.com/Atwix/DPD-Shipping-M2/tree/DPD-280_dpd_returns_improvements

**get param from uiregistry** - https://github.com/Atwix/DPD-Shipping-M2/tree/DPD-334-add-zip-codes-validation


**unit / integration tests** - src/app/code/Niedax/Elbridge