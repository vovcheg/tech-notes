[Index](https://bitbucket.org/vovcheg/tech-notes)

**remove unrefferenced volumes**
```sh
docker volume ls -f dangling=true
docker volume rm <volume name> 
```
**run mariadb container for mage 1924** 
```sh
#create volume if not exist
docker volume create --name mariadbvolumeformage1924 

docker run --name mariadb-for-mage1924 -e MYSQL_ROOT_PASSWORD=Y111111 -p 3309:3306  -v mariadbvolumeformage1924:/mariadbvolumeformage1924 -d mariadb
#start container
docker start mariadbvolumeformage1924

#make backup, attach container that used Volume
docker run --rm --volumes-from mariadb-for-mage1924 -v $(pwd):/backup ubuntu tar czf /backup/var_lib_mysql.tar.gz /var/lib/mysql

#restore backup to Volume
docker run --rm --volumes-from mariadbempty -v $(pwd):/backup ubuntu bash -c "cd /var/lib/mysql && tar xzf /backup/var_lib_mysql.tar.gz --strip 1"
```
**backup volume**
```sh
sudo docker run --rm --volumes-from mysql_test -v $(pwd):/backup ubuntu tar cvf /backup/backup.tar /var/lib/mysql
sudo docker run -v /var/lib/mysql --name dbstore ubuntu /bin/bash
sudo docker run --rm --volumes-from dbstore -v $(pwd):/backup ubuntu bash -c "cd /var/lib/mysql && tar xvf /backup/backup.tar --strip 1"
```
**mongo restore** 
```sh
sudo docker exec -it mongoprod_mongo_prod_1 /bin/bash
mongorestore /var/backup/bu_bqsn_kcc/bu_bqsn_kcc/ --db 'bu_bqsn_kcc'
```
**mongo from scratch**
```bash
#run the following in directory with backup file
docker run -p 27017:27017 -v "$(pwd)":/data --name mongo -d mongo mongod --smallfiles
#connect to container
sudo docker exec -it mongo /bin/bash
mongorestore --db bu_kcctest /data/bu_kcctest/bu_kcctest/
#use 
use bu_kcctest

#delete if needed
mongo bu_kcctest --eval "db.dropDatabase()"
```
**connect to DB in Docker on remote host** 
```sh
ssh vovcheg@192.168.1.105 -L 3307:127.0.0.1:3307 -N
```

**know docker host IP address (for AWS)**
```bash
docker run alpine /bin/sh -c "apk update ; apk add curl ; curl -s http://169.254.169.254/latest/meta-data/local-ipv4 ; echo"
172.31.17.238
```

**How to recreate docker container locally:**
```bash
cat S_LIEBHERR_LPS_MITRY.tar | docker import - liebherr_lps

docker run -t -d -i -h "localhost" -p 4204:80 -p 5204:22 -p 6204:443 --name S_LIEBHERR_LPS_VOVSKY liebherr_lps bash
```

**run komodo container**
```bash
sudo docker run -t -d -i -h "localhost" -p 80:80 -p 2020:22 -p 443:443 --name KOMODO komodo bash
sudo docker run -t -d -i -h "localhost" -p 8080:80 -p 2020:22 -p 443:443 --name KMD kmd bash
docker run -t -d -i -h "localhost" -p 8080:80 -p 2020:22 -p 443:443 --name hypervsn hypervsn bash
```



**docker import/export**
```bash
docker export <container_hash> > filename.tar

cat filename.tar | docker import - <image_name>
```