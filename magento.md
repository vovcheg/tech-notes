[Index](https://bitbucket.org/vovcheg/tech-notes)

- adminhtml form example - atwix channeladvisor
- product options - doubleyou.basiliko.net
- rules and conditions - doubleyou.basiliko.net

*permissions*
```sh
find . -type f -exec chmod 644 {} \;
find . -type d -exec chmod 755 {} \;
```

**select**
```php
//$resource = new Mage_Core_Model_Resource();
$resource = Mage::getSingleton('core/resource');
/** @var Varien_Db_Adapter_Interface $connection */
$connection = $resource->getConnection('core_read');
/** @var Varien_Db_Select $select */
$select = $connection->select();

$resource = Mage::getResourceModel('catalog/product');
$connection = $resource->getReadConnection();
$select = $connection->select();
```
**update value in table**
```php
$connection->update($resource->getTableName('eav_attribute'), array('attribute_code' => 'benefit258_residui'), array('attribute_code = ?' => 'benefit_258_residui'));
```

**save attribute**
```php
$existingCustomer->getResource()->saveAttribute($existingCustomer, $key);

//another
Mage::getSingleton('eav/config')
    ->getAttribute('catalog_product', 'is_benefit')
    ->setData('used_in_product_listing', 1)
    ->save();
```
**get attribute table**
```php
$attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'news_from_date');
$newsTableName = $attributeModel->getBackendTable();

$entityTypeId = Mage::getSingleton('eav/config')->getEntityType(Mage_Catalog_Model_Category::ENTITY)->getId();
$regionIdAttr   = Mage::getSingleton('eav/config')->getAttribute($entityTypeId, 'region_id');
$regionIdTable  = $regionIdAttr->getBackend()->getTable();
```

**collection OR condition**
```php
$collection->addAttributeToFilter(
    array(
        array('attribute'=> 'someattribute','like' => 'value'),
        array('attribute'=> 'otherattribute','like' => 'value'),
        array('attribute'=> 'anotherattribute','like' => 'value'),
    )
);
```
**get cat_id => cat_name**
```phph
$select->from(array('main_table' => $resource->getTable('catalog/category')), array('entity_id'))
            ->joinInner(array('category_name_table' => $attributeTableName),
                sprintf("main_table.entity_id = category_name_table.entity_id AND attribute_id = %s", $attributeModel->getAttributeId()),
                array('category_name' => 'category_name_table.value')
            );
```            

**admin controller**
```php
    <admin>
        <routers>
            <adminhtml>
                <args>
                    <modules>
                        <Atwix_Tweaks before="Mage_Adminhtml">Atwix_Tweaks_Adminhtml</Atwix_Tweaks>
                    </modules>
                </args>
            </adminhtml>
        </routers>
    </admin>
```

```php
$nestedSelect = clone $select;
            $nestedSelect->reset()
                ->from(
                    array('order_grid' => 'sales_flat_order_grid'),
                    array(
                        'entity_id',
                        'uid'
                    )
                );
            $select->join(
                array('orders_grid' => $nestedSelect),
                "orders_grid.entity_id = main_table.entity_id AND orders_grid.uid = $uid",
                array()
            );
```

*root category*
```php
Mage::app()->getStore("default")->getRootCategoryId()
```

**next autoincrement id**
```
app/code/core/Mage/ImportExport/Model/Resource/Helper/Mysql4.php
getNextAutoincrement(
```

*attribute fields mapping app/code/core/Mage/Catalog/Model/Resource/Setup.php*
```php
protected function _prepareValues($attr)
    {
        $data = parent::_prepareValues($attr);
        $data = array_merge($data, array(
            'frontend_input_renderer'       => $this->_getValue($attr, 'input_renderer'),
            'is_global'                     => $this->_getValue(
                $attr,
                'global',
                Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
            ),
            'is_visible'                    => $this->_getValue($attr, 'visible', 1),
            'is_searchable'                 => $this->_getValue($attr, 'searchable', 0),
            'is_filterable'                 => $this->_getValue($attr, 'filterable', 0),
            'is_comparable'                 => $this->_getValue($attr, 'comparable', 0),
            'is_visible_on_front'           => $this->_getValue($attr, 'visible_on_front', 0),
            'is_wysiwyg_enabled'            => $this->_getValue($attr, 'wysiwyg_enabled', 0),
            'is_html_allowed_on_front'      => $this->_getValue($attr, 'is_html_allowed_on_front', 0),
            'is_visible_in_advanced_search' => $this->_getValue($attr, 'visible_in_advanced_search', 0),
            'is_filterable_in_search'       => $this->_getValue($attr, 'filterable_in_search', 0),
            'used_in_product_listing'       => $this->_getValue($attr, 'used_in_product_listing', 0),
            'used_for_sort_by'              => $this->_getValue($attr, 'used_for_sort_by', 0),
            'apply_to'                      => $this->_getValue($attr, 'apply_to'),
            'position'                      => $this->_getValue($attr, 'position', 0),
            'is_configurable'               => $this->_getValue($attr, 'is_configurable', 1),
            'is_used_for_promo_rules'       => $this->_getValue($attr, 'used_for_promo_rules', 0)
        ));
        return $data;
    }
```

**load layout handle and return html**
```php
        $layout = Mage::app()->getLayout();
        $update = $layout->getUpdate();
        $update->load('checkout_onepage_paymentmethod');
        $layout->generateXml();
        $layout->generateBlocks();
        $output = $layout->getOutput();
        return $output;

```

**add column to table and updatefromselect**
```php
$installer->getConnection()->addColumn(
    $installer->getTable('sales/order_grid'),
    'shipping_address',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => true,
        'default' => NULL,
        'comment' => 'Shipping Address'
    )
);

$resource = Mage::getSingleton('core/resource');
$connection = $resource->getConnection('core_read');
$select = $connection->select();
$select
    ->from(
        array(
            'address_table' => $installer->getTable('sales/order_address')
        ),
        array(
            'shipping_address' => Mage::helper('starshop_grids')->getAddressFieldConcat()
        )
    )
    ->where('address_table.parent_id = main_table.entity_id AND address_table.address_type = "shipping"');

$updateQuery = $connection->updateFromSelect($select, array('main_table' => $installer->getTable('sales/order_grid')));
$connection->query($updateQuery);
```
**save config**
```php
Mage::getConfig()->saveConfig('turpentine_varnish/general/ajax_messages', 0);
Mage::getConfig()->reinit();
Mage::app()->reinitStores();
```
**quick backup magento default files**
```sh
tar -czf var/backup16_05_16.tar.gz api.php app cron.php cron.sh downloader errors favicon.ico get.php index.php index.php.sample info.php install.php js lib mage php.ini.sample pkginfo scheduler_cron.sh shell skin --exclude='skin/frontend/default/wfs/images/*.pdf'
```

**m2**
```sh
tar -czf backup/2018-12-05.tar.gz public_html --exclude='public_html/var'  --exclude='public_html/pub' --exclude='public_html/setup' --exclude='public_html/generated'`
```

**add column to existing table**

```php
$installer->getConnection()->addColumn($this->getTable('celebrity/product'), 'role_name', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable' => true,
    'default' => NULL,
    //comment
    //primary
    //'length' => '12',
    //'length' => '5,2',
    //IDENTITY
    //AFTER
));

//lib/Varien/Db/Adapter/Pdo/Mysql.php:addColumn();


```
**join attribute to collection**
```php
 $categoryCollection = Mage::getModel('catalog/category')->getCollection();

        $adminStore = Mage_Core_Model_App::ADMIN_STORE_ID;
        $categoryCollection->joinAttribute(
            'name',
            'catalog_category/name',
            'entity_id',
            null,
            'inner',
            $adminStore
        );
```
**Unique key and foreign key/CONSTRAINT**
```php
$installer->getConnection()
//....
->addIndex(
        $installer->getIdxName(
            'celebrity/royalty_rate',
            array('order_item_id', 'celebrity_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('order_item_id', 'celebrity_id'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
    ->addForeignKey($installer->getFkName('celebrity/royalty_rate', 'order_item_id', 'sales/order_item', 'item_id'),
        'order_item_id', $installer->getTable('sales/order_item'), 'item_id',
        Varien_Db_Ddl_Table::ACTION_SET_NULL, Varien_Db_Ddl_Table::ACTION_CASCADE)
```


**check if class exists**
```php
if (@class_exists('Enterprise_ImportExport_Model_Import_Entity_Product')) {
    abstract class AvS_FastSimpleImport_Model_Import_Entity_Product_Abstract extends Enterprise_ImportExport_Model_Import_Entity_Product {}
} else {
    abstract class AvS_FastSimpleImport_Model_Import_Entity_Product_Abstract extends Mage_ImportExport_Model_Import_Entity_Product {}
}


class AvS_FastSimpleImport_Model_Import_Entity_Product extends AvS_FastSimpleImport_Model_Import_Entity_Product_Abstract
{
```
**get atribute raw value**
```php
$_item = $this->getProduct()->getId();
$_resource = $this->getProduct()->getResource();
$optionValue = $_resource->getAttributeRawValue($_item, 'custom_attribute_value', Mage::app()->getStore());
```

**set indexer mode**
```
$processes = array();
$indexer = Mage::getSingleton('index/indexer');
foreach ($indexer->getProcessesCollection() as $process) {
    //store current process mode
    $processes[$process->getIndexerCode()] = $process->getMode();
    //set it to manual, if not manual yet.
    if($process->getMode() != Mage_Index_Model_Process::MODE_MANUAL){
        $process->setData('mode','manual')->save();
    }
}

//reindex all and set indexes as they were
foreach ($indexer->getProcessesCollection() as $process) {
    //$process->reindexEverything();
    $process->setData('mode',$processes[$process->getIndexerCode()])->save();
}
```

**measure unstall scripts**
```
//file app/code/core/Mage/Core/Model/Resource/Setup.php
\Mage_Core_Model_Resource_Setup::_upgradeResourceDb
                switch ($fileType) {
                    case 'php':
                        $conn   = $this->getConnection();
                        $start = microtime(true);

                        $result = include $fileName;
                        $time_elapsed_secs = microtime(true) - $start;
                        file_put_contents('/Users/vovcheg/repos/star-shop.magento/var/log/starsshop_install_script2.log', $time_elapsed_secs . ' '. $fileName .
                         PHP_EOL, FILE_APPEND);

                        Mage::log($time_elapsed_secs . ' '. $fileName, null, 'starsshop_install_script.log');
                        Mage::log($time_elapsed_secs . ' '. $fileName);
                        break;
                    case 'sql':

```
**run cron job**
```
require_once('../app/Mage.php');
umask(0);
Mage::app();
$key = 'starshop_vendors';
$helper = Mage::helper('aoe_profiler');
$schedule = Mage::getModel('cron/schedule')
    ->setJobCode($key)
    ->setScheduledReason(Aoe_Scheduler_Model_Schedule::REASON_RUNNOW_WEB)
    ->runNow(false)// without trying to lock the job
    ->save();

$messages = $schedule->getMessages();

if ($schedule->getStatus() == Mage_Cron_Model_Schedule::STATUS_SUCCESS) {
    echo($helper->__('Ran "%s" (Duration: %s sec)', $key, intval($schedule->getDuration())));
    if ($messages) {
        echo($helper->__('"%s" messages:<pre>%s</pre>', $key, $messages));
    }
} else {
    echo($helper->__('Error while running "%s"', $key));
    if ($messages) {
        echo($helper->__('"%s" messages:<pre>%s</pre>', $key, $messages));
    }
}
```

**modify form element**
```
parent::_prepareForm();
        $form = $this->getForm();

        /** @var Varien_Data_Form_Element_Fieldset $fieldset */
        $fieldset = $form->getElement('action_fieldset');
        foreach ($fieldset->getElements() as $element) {
            if ($element->getName() == 'simple_action') {
                $element->setOptions($element['options'] + array('cost_percent' => 'Cost + %'));
                $values = $element->getValues();
                $values[] = array('value' => 'cost_percent', 'label' => 'Cost + %');
                $element->setValues($values);
            }
        }
```        

**class extends, check existance**
```
app/code/community/AvS/FastSimpleImport/Model/Import/Entity/Product.php
```

**redirect**
```php
 $controller->getRequest()->setDispatched(true);
            $controller->setFlag(
                '',
                Mage_Core_Controller_Front_Action::FLAG_NO_DISPATCH,
                true
            );
            $controller->getResponse()->setRedirect(Mage::getBaseUrl());
```

**get customer value from DB**
```sql
SELECT *
FROM customer_entity_varchar
  INNER JOIN eav_attribute ON
                             customer_entity_varchar.attribute_id = eav_attribute.attribute_id
  INNER JOIN customer_entity ON customer_entity.entity_id = customer_entity_varchar.entity_id
WHERE attribute_code = 'external_id' AND email = 'qatestdp7@gmail.com';
```

**add attribute to all attribute sets**
```php
$entityType = Mage::getModel('catalog/product')->getResource()->getEntityType();
$attributeId = Mage::getModel('eav/entity_attribute')->loadByCode($entityType, 'manufacturer')->getAttributeId();
$groupName = 'General';
$collection = Mage::getResourceModel('eav/entity_attribute_set_collection')
    ->setEntityTypeFilter($entityType->getId());

foreach ($collection as $attributeSet) {
    $attributeGroup = $installer->getAttributeGroupId('catalog_product', $attributeSet->getId(), $groupName);
    $installer->addAttributeToSet('catalog_product', $attributeSet->getId(), $attributeGroup, $attributeId);
}
```
**move attribute from one group to other**
```php
$installer = new BuyerQuest_Supplier_Model_Resource_Setup('core_setup');

$oldGroupName = 'Additional Attributes';
$newGroupName = 'Client Attributes';

$oldGroupId = $newGroupId = null;

$installer = new Mage_Catalog_Model_Resource_Eav_Mysql4_Setup('core_setup');
$entityTypeId = $installer->getEntityTypeId('catalog_product');
$attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
$attributeGroupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection');

$groups = Mage::getModel('eav/entity_attribute_group')
    ->getResourceCollection()
    ->setAttributeSetFilter($attributeSetId)
    ->setSortOrder()
    ->load();

foreach ($groups as $group) {
    if ($group->getAttributeGroupName() == $oldGroupName) {
        $oldGroupId = $group->getId();
    } elseif ($group->getAttributeGroupName() == $newGroupName) {
        $newGroupId = $group->getId();
    }
}
if ($oldGroupId && $newGroupId) {
    $attributes = Mage::getResourceModel('eav/entity_attribute_collection')
        ->setAttributeGroupFilter($oldGroupId)->setEntityTypeFilter($entityTypeId);

    foreach ($attributes as $attribute) {
        $installer->addAttributeToGroup($entityTypeId, $attributeSetId, $newGroupId, $attribute->getId());
    }
}
```

**update product attribute**
```php
$allProductIds = Mage::getModel('catalog/product')->getCollection()->getAllIds();
	$attributeCode = 'some_eav_attribute';
	$attrData[$attributeCode] = 'some_value';
	$storeId = 0;
	Mage::getSingleton('catalog/resource_product_action')->updateAttributes($allProductIds, $attrData, $storeId);
```
**get backend table**
```php
$sourceSystemIdAttribute = Mage::getModel('eav/config')->getAttribute('supplier', self::COL_ID);
$sourceSystemIdAttributeTable = $sourceSystemIdAttribute->getBackendTable();
```            
**add admin user**
```sql
LOCK TABLES `admin_role` WRITE , `admin_user` WRITE;

SET @SALT = "rp";
SET @PASS = CONCAT(MD5(CONCAT( @SALT , "bquest01") ), CONCAT(":", @SALT ));
SELECT @EXTRA := MAX(extra) FROM admin_user WHERE extra IS NOT NULL;

INSERT INTO `admin_user` (firstname,lastname,email,username,password,created,lognum,reload_acl_flag,is_active,extra,rp_token_created_at)
VALUES ('dev','test','test@buyrequest.con','devtest',@PASS,NOW(),0,0,1,@EXTRA,NOW());

INSERT INTO `admin_role` (parent_id,tree_level,sort_order,role_type,user_id,role_name)
VALUES (1,2,0,'U',(SELECT user_id FROM admin_user WHERE username = 'devtest'),'dev');

UNLOCK TABLES;
```



**xdebug cli shell script**
```bash
php -dxdebug.remote_enable=1 -dxdebug.remote_autostart=On -dxdebug.idekey=PHPSTORM descriptionReplace.php
```

**forward noroute**
```php
$request->initForward();
$request->setControllerName('index');
$request->setModuleName('cms');
$request->setActionName('noRoute')
    ->setDispatched(false);
```

```bash
n98-magerun.phar sys:setup:remove atwix_dpd && n98-magerun.phar sys:setup:run

```