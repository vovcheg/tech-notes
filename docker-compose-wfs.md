[Index](https://bitbucket.org/vovcheg/tech-notes)

**Docker Compose for Magento, etc**

```yml
version: '2'
services:
  nginx:
    image: nginx:latest
    ports:
     - "8080:80"
    volumes:
     - ./source:/source
     - ./nginx.conf:/etc/nginx/conf.d/default.conf
    depends_on:
     - php
  php:
    build: ./php
    environment:
       XDEBUG_CONFIG: "remote_host=172.17.0.1"
    volumes:
     - ./source:/source
     - ./logs:/tmp/xdebug_log
  db:
    image: mariadb:latest
    ports:
     - "3306:3306"
    volumes:
       - db_data:/var/lib/mysql
    environment:
      MYSQL_DATABASE: 'wfs'
      MYSQL_USER: 'root'
      MYSQL_PASSWORD: 'wfs'
      MYSQL_ALLOW_EMPTY_PASSWORD: 'yes'
  magerun:
    image: meanbee/magerun
    environment:
      MAGE_ROOT_DIR: /source
    links:
      - db
    volumes_from:
      - nginx    
volumes:
  db_data:

```