[Index](https://bitbucket.org/vovcheg/tech-notes)

# PV:
`pv bd_bqsn_cs_2017-04-13.sql.gz | gunzip | mysql -u root -h 127.0.0.1 -P 3307 -f bd_bqsn_cs`
`mysqldump [database] | pv | gzip -c > [file].sql.gz`

**restart ssh Ubuntu**
```bash
sudo /etc/init.d/ssh restart
```

**stop solr nohup**
```bash
#look for "nohup"
ps -ef | grep nohup
sudo kill -9
```

**ssh tonnel for mysql**
```bash
ssh vovcheg@93.77.1.94 -L 3305:127.0.0.1:3305 -N
ssh developer5@23.253.123.187 -L 3306:127.0.0.1:3305 -N
ssh developer5@174.143.164.45 -L 3305:127.0.0.1:3306
ssh developer5@174.143.164.45 -4 -L 3305:127.0.0.1:3306


ssh developer5@174.143.164.45 -4 -L 3304:127.0.0.1:3306
mysql -P 3304 -uheadcovers  -p -h 127.0.0.1
```
**backup default magento files**
```sh
tar -czf var/backup16_05_16.tar.gz api.php app cron.php cron.sh downloader errors favicon.ico get.php index.php index.php.sample info.php install.php js lib mage php.ini.sample pkginfo scheduler_cron.sh shell skin --exclude='skin/frontend/default/wfs/images/*.pdf'
```
**find and kill solr, then run**
```bash
ps aux | grep solr
cd /opt/solr36/example
java -Dsolr.solr.home=/opt/solr36/example/bw_kucorp -jar start.jar
```
**mysql import over ssh pipe**
```bash
ssh -p 5104 dev@bravo.atwix.com "mysqldump -u root -p111 dpd" | mysql -u root -p111 dpd
ssh -p 5205 -i ~/.ssh/id_rsa_vovsky dev@bravo.atwix.com "mysqldump -u root -p111 headcovers_live" | mysql -u root -p111 headcovers_20190822
```

**rsync upload file**
```bash
rsync -razhv dpd_m2.tar -e 'ssh -p 5202' dev@bravo.atwix.com:/home/dev/sites/
rsync -razhv /home/dev/sites/niedax -e 'ssh -p 5215' dev@alpha.atwix.com:/home/dev/sites/niedax
rsync -razhv /home/dev/sites/1percent -e 'ssh -p 5218' dev@alpha.atwix.com:/home/dev/sites/1percent
rsync -razhv /extrahd/docker_backups/mageclean_nginx.tar -e 'ssh -p 40404 -i /home/vovsky/.ssh/id_rsa' vovsky@bravo.atwix.com:/extrahd/docker_backups/mageclean_nginx.tar

ssh -p 5215 dev@alpha.atwix.com -i ~/.ssh/id_rsa_atwix
```

**rsync download files**
```bash
rsync -avz -e 'ssh -i ~/.ssh/id_rsa_foodsafety_vovsky' root@138.197.135.37:/var/www/IEA/IEA-MAG/ /Users/vovsky/repos/IEA-MAG/ --exclude="/var" --exclude="/media"
```
**folder size recursively**
```
du -h ./ -d 1
```

**restart ssh port forwarding xdebug**
```bash
sudo netstat -plant  | grep 9000
sudo kill -9 553
```

quicly rsync only needed files:
```bash
rsync -avz -e 'ssh -p 5200 -i ~/.ssh/id_rsa_atwix' dev@alpha.atwix.com:/home/dev/sites/bravo/commerce/ /Users/vovsky/repos/commerce232/ --exclude="/.git" --exclude="/var" --exclude="/pub/static" --exclude="magento2-sample-data" --exclude="/dev" --exclude="/setup" --exclude="/update" --exclude="/vendor/magento/magento2-base/dev"
```

do not store command in history:
```bash
export HISTCONTROL=ignorespace
```