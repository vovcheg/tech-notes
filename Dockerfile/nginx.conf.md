[Index](https://bitbucket.org/vovcheg/tech-notes)

```apacheconfig
server {
    index index.php index.html;
    server_name 93.77.1.94;
    error_log  /var/log/nginx/error.log;
    access_log /var/log/nginx/access.log;
    root /source;

    location / {  
        proxy_read_timeout 6000;
        try_files $uri $uri/ /index.php?$args;
    }  

   
    
    location /cs {
        #root /source/cs
        index   index.php;
        try_files $uri $uri/ /cs/index.php?$args;
    }
    
    location /ku {
        #root /source/ku
        index   index.html index.htm index.php;
        try_files $uri $uri/ /ku/index.php?$args;
    }

    location /disney {
            #root /source/ku
            index   index.html index.htm index.php;
            try_files $uri $uri/ /disney/index.php?$args;
        }

    location /united {
        index   index.html index.htm index.php;
        try_files $uri $uri/ /united/index.php?$args;
    }
    
     location /mcd {
            index   index.html index.htm index.php;
            try_files $uri $uri/ /mcd/index.php?$args;
        }
        
    location /wendys {
            index   index.html index.htm index.php;
            try_files $uri $uri/ /wendys/index.php?$args;
        }
    
    location ~ \.php$ {
        proxy_read_timeout 6000;
        proxy_connect_timeout       600;
        proxy_send_timeout          600;
        #send_timeout                600;
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass php:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_read_timeout 600;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }

    # Enable Gzip
  gzip  on;
  gzip_http_version 1.0;
  gzip_comp_level 2;
  gzip_min_length 1100;
  gzip_buffers     4 8k;
  gzip_proxied any;
  gzip_types
    # text/html is always compressed by HttpGzipModule
    text/css
    text/javascript
    text/xml
    text/plain
    text/x-component
    application/javascript
    application/json
    application/xml
    application/rss+xml
    font/truetype
    font/opentype
    application/vnd.ms-fontobject
    image/svg+xml;

  gzip_static on;

  gzip_proxied        expired no-cache no-store private auth;
  gzip_disable        "MSIE [1-6]\.";
  gzip_vary           on;
}


```