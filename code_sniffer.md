hey guys, I would like to tell how to fix some **Travis errors** in your pull requests.
First, read Yaro's article and configure **code sniffer** and **mass detector** https://www.atwix.com/magento-2/configure-code-sniffer-for-phpstorm/. 
**Note** that path to Code Sniffer validation rules has been changed, it's:
`dev/tests/static/framework/Magento/ruleset.xml`. 
It will help you avoid errors when you code.
However it could be still not enough and travis fail. In such case try to use command line tool
that comes with Magento **phpcbf**. I could not find find good way to integrate in with PHPStorm, 
but you can run it from command line. 
**Example**. We need to fix errors in app/code/Magento/CatalogGraphQl/Model/Resolver/Products/Attributes/Collection.php:

`./vendor/squizlabs/php_codesniffer/bin/phpcbf --standard=/path/to/project/dev/tests/static/framework/Magento/ruleset.xml /path/to/project/app/code/Magento/CatalogGraphQl/Model/Resolver/Products/Attributes/Collection.php`

  
