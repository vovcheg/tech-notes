##FPC

Depersonalization Plugins are not a way to add private data to a page, that’s just something Magento does to avoid private data from ending up in the page cache.
The three approaches I know are private scope blocks, customer data sections or home grown Ajax response rendering.