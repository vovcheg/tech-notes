[toc]

### RabbitMQ

Consumer, Publisher.

### Patterns used in Magento 2
 - service contracts
 - Dependency injection
 - Factory
 - Event Observer
 - Proxy
 - Plugins

**Service contracts**. A service contract is a set of interfaces which act as a layer between an end user and business layer. Thus rather than directly exposing business logic for customization to end user, a layer called service contract comes in between.
Service contracts enhances the modularity of Magento.
Helps merchants for easy upgrade of Magento
Ensure well-defined and durable API that other external and Magento module implements.
Provide an easy way to expose business logic via REST or SOAP interfaces.
 A service contract includes data interfaces, which preserve data integrity, and service interfaces, which hide business logic details from service requestors such as controllers, web services, and other modules.
 
**Dependency injection** - injecting the dependent object through external environment rather than creating them internally.
Benefit - writing tests. This allows for loose coupling of code because object A no longer needs to be concerned with initializing its own dependencies. Object B decides which implementations to provide to object A based on a configuration or desired behavior.

**Factory** - are service classes that instantiate non-injectable classes, that is, models that represent a database entity . They create a layer of abstraction between the ObjectManager and business code.

**Event Observer** - Events are a way to hook up your functionality in between of code without overriding any core class.
Using events and observers, you can run your custom code in response to a specific Magento event or even a custom event.
 
**Proxy**

A proxy allows a class to be loaded at a later point in time. Additionally, proxies can be used to break a circular dependency loop. Constructor injection also means that a chain reaction of object instantiation is often the result when you create an object.
Lazy loading.


### Rest API
Three (4) types of authentication: guest, admin user/pass, cusstomer user/pass, Session-based authentication, OAuth-based. webapi.xml. webapi.xml — definition of REST routes, HTTP verbs, interfaces/methods they will execute.
Example: 
```
<route url="/V1/products" method="POST">
    <service class="Magento\Catalog\Api\ProductRepositoryInterface" method="save"/>
    <resources>
        <resource ref="Magento_Catalog::products" />
    </resources>
</route>
```

### Rest/SOAP
**SOAP**. Defines how messages are sent. Successful/retry logic for reliable messaging functionality. SOAP has tighter security. Only XML. Supports WS security. Can not be cached. Need to know everything bvefore the interraction.
**REST** is architectual styles rather then protocol. Support other types: JSON, XML, HTML. Can be cached. REST APIs access a resource for data (a URI); SOAP APIs perform an operation. SOAP requires more bandwidth; REST requires fewer resources

### What is in di.xml

**Virtual types** - A virtual type allows you to change the arguments of a specific injectable dependency and change the behavior of a particular class. This allows you to use a customized class without affecting other classes that have a dependency on the original.  This allows Magento\Framework\Session\Generic to be customized without affecting other classes that also declare a dependency on Magento\Framework\Session\Storage

**Constructor arguments** - You can configure the class constructor arguments in your di.xml in the argument node.

**Abstraction-implementation mappings** - The object manager uses abstraction-implementation mappings when the constructor signature of a class requests an object by its interface. The object manager uses these mappings to determine what the default implementation is for that class for a particular scope **(preference)**.

**Object lifestyle** (Singleton/Transient) - The shared property determines the lifestyle of both argument and type configurations.

### Routing 

index.php -> HTTP application -> FrontController -> Routing (routers match) -> Controller processing -> etc
some of routers: Base Router → CMS Router → UrlRewrite Router → Default Router

### EAV 

EAV is used when you have many attributes for an entity and these attribute are dynamic (added/removed).
Also there is a high possibility that many of these attribute would have empty or null value most of the time.
In such a situation EAV structure has many advantages mainly with optimized mysql storage.
Disadvantages of EAV is mainly speed since it require multiple sql queries across tables to perform any operation.

### Depersonalization

 Public – public content is stored server-side in your reverse proxy cache storage;
 Private – private content is stored client side and is specific to an individual customer. Magento cleans the session storage for cacheable requests in order to avoid caching of customer private content.

### Vault
Vault as a payment method provides store customers with ability to use the previously saved credit card information for checkout. This information is stored safely on the side of trusted payments gateways (Braintree, PayPal). Not storing the sensitive credit card information is one of the PCI compliance requirements.

### bulk API
**Bulk API** endpoints differ from other REST endpoints in that they combine multiple calls of the same type into an array and execute them as a single request. The endpoint handler splits the array into individual entities and writes them as separate messages to the message queue.

### UI components



### Database replication

Setting up database replication provides the following benefits:

- Provides data backup
- Enables data analysis without affecting the master database
- Scalability

The advantage of this setup is that Magento can issue read queries to any of the Slave servers, saving all the write queries for the Master database

### private content, sections
Private content is not stored server-side but on the client side only. To generate customer private data you need to use CustomerData sections.

To add private content, you must first define a section source (di.xml). The sections themselves are defined within a sections.xml. Requires a jsLayout component argument t


### Request
Скаладається з 3х частин: Request/Start Line (GET / HTTP/1.1),  Headers(User-Agent, Host, Accept, Accept-Encoding, cookies), Body

### Response
Скаладається з 3х частин: Start Line (STATUS, etc),  Headers(content type), Body

### Sessions
An alternative way to make data accessible across the various pages of an entire website is to use a PHP Session.

A session creates a file in a temporary directory on the server where registered session variables and their values are stored. This data will be available to all pages on the site during that visit. A cookie called PHPSESSID is automatically sent to the user's computer to store unique session identification string.

### Cookies
An HTTP cookie (also called web cookie, Internet cookie, browser cookie, or simply cookie) is a small piece of data sent from a website and stored on the user's computer by the user's web browser while the user is browsing

### Magic methods
The "magic" methods are ones with special names, starting with two underscores, which denote methods which will be 
triggered in response to particular PHP events.

__toString() called within function  like "echo",  __clone() - on object copy, __construct() - when object is created, 
__call() is triggered when invoking inaccessible methods in an object context, __get() - when you try to read inaccessible
property, __set() - when you try to set inaccessible property.


### right left join
Select * from Table1 left join Table 2 will return ALL the records of table 1 plus coincident records of Table 2. 
The opposite Select * from Table1 right join Table 2would return ALL records from Table 2 and coincident records of Table 1.

### MVC, knockout MVC. Where the business logic is?

**Model**
The Model component corresponds to all the data-related logic that the user works with. This can represent either the 
data that is being transferred between the View and Controller components or any other business logic-related data. 
For example, a Customer object will retrieve the customer information from the database, manipulate it and update it
 data back to the database or use it to render data.

**View**
The View component is used for all the UI logic of the application. For example, the Customer view will include 
all the UI components such as text boxes, dropdowns, etc. that the final user interacts with.

**Controller**
Controllers act as an interface between Model and View components to process all the business logic and incoming requests,
 manipulate data using the Model component and interact with the Views to render the final output. For example, the 
 Customer controller will handle all the interactions and inputs from the Customer View and update the database using the 
 Customer Model. The same controller will be used to view the Customer data.

**knockout** The main thing to keep in mind is that the Model is the data and the View Model is the layer that makes your
 Model available to the view (usually via data-binding). Sometimes the Model may be a separate class; 


### innodb vs myisam

The main difference between MyISAM and INNODB are:

- MyISAM does not support transactions by tables while InnoDB supports.
- MyISAM does not support FOREIGN-KEY referential-integrity constraints while InnoDB supports.
- InnoDB does not support FULLTEXT index (now supports) while MyISAM supports.
- Performance speed of MyISAM table is much higher as compared with tables in InnoDB.

I see that all Magento 2 tables are innodb

### Types of indexes in MySQL

- Single and Multiple Column Indexes

1. A **unique** index is one in which all column values must be unique
2. A **primary** key is a unique index in which no value can be NULL. 
3. A **simple, regular, or normal** index is an index where the values don’t need to be unique and they can be NULL. This is the index I’ve mostly been talking about to this point. They’re added simply to help the database find things faster.
4. A **fulltext** index, as the name implies, are used for full text searches. Sometimes you want to find the blob of text that contains a certain word or group of words or maybe you want to find a certain substring within the larger block of text.
 
 Implemetattion details: b-tree, hash. Hash indexes can be used for equality comparisons that use the = or <=> operators. They’re very fast, but they aren’t good at finding ranges of values, only exact values and they can’t be used to sort results through ORDER BY.

https://vanseodesign.com/web-design/the-types-of-indexes-you-can-add-to-mysql-tables/

### інтерфейси

інтерфейси можуть містити константи, в імплементації костанти не можна перевизначити. Може наслідувати інші інтерфейси,
 інтерфейс не може містити:
- Любые свойства
- Непубличные методы
- Методы с реализацией
- Непубличные константы

### множественное наследование

Да, в случае интерфейсов. Интерфейс может наследоваться от нескольких других интерфейсов.

### абсрактные класы vs интерфейсы

Абстрактный класс не может иметь экземпляров. Интерфейс, в отличие от абстрактного класса, 
не может содержать поля и методы, имеющие реализацию. Если в классе, который реализует интерфейс, не реализованы все 
методы интерфейса, то он должен быть абстрактным. Один класс может реализовывать сколь угодно много интерфейсов.

### Статические переменные

Локальную переменную, которая "помнит" свое значение между вызовами функции. Такая переменная  называется статической.

### protected / private

Мы можем переопределить public и protected методы, но не private.
**private** scope when you want your variable/function to be visible in its own class only.
**protected** scope when you want to make your variable/function visible in all classes that extend current class including the parent class.

### SPL

Standard PHP Library.  Collection of interfaces and classes that are meant to solve common problems: iterators, interfaces, Exceptions. (SplStack, SplQueue, LogicException)


### SOLID
- **Single Responsibility** Принцип единой отвественности На каждый объект должна быть возложена одна единственная обязанность.
- **Open Closed** Принцип открытости и закрытости Программные сущности должны быть открыт для расширения, но закрыты для модификации. Software entities (classes, modules, functions, etc.) be extendable without actually changing the contents of the class you’re extending
- **Liskov Substitution** Принцип подстановки Барбары Лисков Функции, которые используют базовый тип, должны иметь возможность использовать подтипы базового типа, не зная об этом. It states that any implementation of an abstraction (interface) should be substitutable in any place that the abstraction is accepted. Basically, it takes care that while coding using interfaces in our code, we not only have a contract of input that the interface receives but also the output returned by different Classes implementing that interface; they should be of the same type.
- **Interface Segregation** Принцип разделения интерфейсов Много специализированных интерфейсов лучше, чем один универсальный. 
-  **Dependency Inversion** Инверсия Зависимости. Зависимости должны строится относительно абстракции, а не деталий Детали должены зависеть от абстракции, но абстракция не должна зависть от деталей.  Depend on Abstractions not on concretions.



### Q&A:
- может ли клас имплементировать несколько интерфейсов?
    - yes
- compilation
    - di:compile (factories, proxies, interceptors, etc)
- DRY
    - do not repeat yourself

### Minor, not described:
- entity manager/hydrator
- priority of order, limit, goup, having
- purpose of extension attributes
- clone() singleton
- dynamic rows
