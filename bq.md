[Index](https://bitbucket.org/vovcheg/tech-notes)

**loggly**
```bash
tag:"magento.exception.var.www.www.buyerquest.net.kucorp.var.log.exception.log"
tag:"magento.system.var.www.www.buyerquest.net.kucorp.var.log.system.log"
```

**truncate tables**
```sql
truncate table buyerquest_invoice_report;
truncate table report_event;
truncate table buyerquest_history;
truncate table buyerquest_cxml_asn_report;
truncate table log_url_info;
truncate table log_url;
truncate table punchout_punchout_transaction;
truncate table punchout_webclient_transaction;
truncate table aschroder_email_log;
```
```sql
UPDATE core_config_data
set value='http://93.77.1.94:8888/ku/' 
WHERE path in ('web/unsecure/base_url', 'web/secure/base_url', 'admin/url/custom', 'web/secure/base_url', 'web/unsecure/base_url');
```