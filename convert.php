<?php

$path = $argv[1];

if ($path) {
    //$template = "convert %s -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace sRGB -resize 271x152 %s";
    $template = "convert %s -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace sRGB %s";
    if (substr($path, -1) != '/') {
        $images = [$path];
    } else {
        $images = glob($path . '*.jpg');
    }

    $output = '';
    foreach ($images as $image) {
        $image = str_replace(' ', '\ ', $image);
        $output .= sprintf($template, $image, $image) . '; \\' . PHP_EOL;
    }
    echo $output;
} else {
    echo "no imgae";
}
