It's not recommended to add other methods with own semantic to the **Repository** interface, those methods recommended to put into some dedicated Services.

**API and SPI segregation**

On MSI project we decided to segregate API (Application Programming Interfaces) from SPI (Service Provider Interfaces) for better customization and decreasing coupling of components.

Repository now could be considered as an API - Interface for usage (calling) in the business logic
Separate class-commands to which Repository proxies initial call (like, Get Save GetList Delete) could be considered as SPI - Interfaces that you should extend and implement to customize current behavior
For example if you look at the implementation of Magento\Inventory\Model\StockRepository you would see that Repository constructor accepts a list of Commands for each of the operation it provides as a part of public interface. And just proxies execution to needed command when one called its public method.