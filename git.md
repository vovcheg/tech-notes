[Index](https://bitbucket.org/vovcheg/tech-notes)

```sh
git diff-tree -r --name-only master QM-252 | xargs tar -rf var/qm252.tar
git diff-tree -r --name-only master shopping_list_table | xargs tar -rf var/shopping_list_2019_02_10.tar
git diff-tree -r --name-only remote_staging EL-241_staging | xargs tar -rf var/EL-241.tar
```

**two commits diff**
```
git archive -o var/latest.zip some-commit $(git diff --name-only earlier-commit some-commit)
git archive -o var/shopping_list.zip 601134a4871f7bcfc52db4ae398f81ded4f00438 $(git diff --name-only 12c7d6bd28eaa29b1d0aeca954292155696f4e5c 601134a4871f7bcfc52db4ae398f81ded4f00438)
git archive -o var/zip_code.zip f84458e3d2a7e6523c5f79af1ba00077bd6c97ca $(git diff --name-only 2f26caf3fbb671b3d8b46769a5f1e8aa5824e041 f84458e3d2a7e6523c5f79af1ba00077bd6c97ca)
git archive -o var/sl-02-01.zip f84458e3d2a7e6523c5f79af1ba00077bd6c97ca $(git diff --name-only 238c14b13911f655a0687c6f7b3f5ab83eed520f f84458e3d2a7e6523c5f79af1ba00077bd6c97ca)
git archive -o var/el-241.zip 35daff2515ecf1d15ab802feca475be60c8ce3dd $(git diff --name-only 34f5734e12aa8d0a1acdda73a4ab19145ae83d18 35daff2515ecf1d15ab802feca475be60c8ce3dd)
git archive -o var/requisition.zip e701c053743bf7a941fa9f9ac6507f6c43b1a164 $(git diff --name-only 65ea2022b18d3325fa20d6f90123d170d500eff1 e701c053743bf7a941fa9f9ac6507f6c43b1a164)
git archive -o var/requisition.zip e701c053743bf7a941fa9f9ac6507f6c43b1a164 $(git diff --name-only abb2450086ae0e1331bdc8ba1b6ac9b400935236 e701c053743bf7a941fa9f9ac6507f6c43b1a164)
git archive -o var/requisition.zip abb2450086ae0e1331bdc8ba1b6ac9b400935236 $(git diff --name-only 3166093563161d58de80c7ae346a1449fed15342 abb2450086ae0e1331bdc8ba1b6ac9b400935236)
```

**rename branch**
*If you want to rename a branch while pointed to any branch, simply do :*
```
git branch -m <oldname> <newname>
```
*If you want to rename the current branch, you can simply do:*
```
git branch -m <newname>
```

**delete branch**
```
git branch -D the_local_branch // local
git push origin --delete <branchName> //remote
git push origin :<branchName>
```

**filter Git files**
```bash
git diff --name-only --diff-filter=A --cached # All new files in the index  
git diff --name-only --diff-filter=A          # All files that are not staged  
git diff --name-only --diff-filter=A HEAD     # All new files not yet committed
```

**checkout remote branch**
```
git checkout -t remote_name/remote_branch
```

**stage hunks by reg exp**
```$bash
git diff -U0 | grepdiff 'www.magentocommerce.com' --output-matching=hunk | git apply --cached --unidiff-zero
#patchutils has a command grepdiff that can be use to achieve this.
#I use -U0 on the diff to avoid getting unrelated changes. You might want to adjust this value to suite your situation.


git diff -U0 | grepdiff '@copyright.*[Cc]opyright' --output-matching=hunk  | git apply --cached --unidiff-zero
git diff -U0 | grepdiff 'license@magento.com' --output-matching=hunk | git apply --cached --unidiff-zero
git diff -U0 | grepdiff 'www.magentocommerce.com' --output-matching=hunk | git apply --cached --unidiff-zero
```

**diff two branches, names only**
```bash
git diff ..develop --name-status > www/var/diff.txt
git diff ..develop --name-status | grep php > www/var/diff.txt
```

**get new/modified files names**
```bash
git diff --name-only --diff-filter=A --cached > var/new_files.txt
git diff --name-only --diff-filter=M --cached > var/modified.txt
```

**save credentials for 333 days 
```
git config --global credential.helper 'cache --timeout 28800000'
```



**if files stuck in index**

`git ls-files -m | xargs -i git update-index --assume-unchanged "{}"`

On Mac OSX, however xargs operates a little bit different (thx Daniel for the comment):

`git ls-files -m | xargs -I {} git update-index --assume-unchanged {}`


**set username and email**
```bash
git config user.name "Volodymyr Vygovskyi" --global && git config user.name "Volodymyr Vygovskyi" && git config user.email "v.vygovskyi@atwix.com" --global && git config user.email "v.vygovskyi@atwix.com" 

```

